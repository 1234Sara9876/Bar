﻿namespace Bar
{
    partial class dispensador
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(dispensador));
            this.buttonBase = new System.Windows.Forms.Button();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.progressB = new System.Windows.Forms.ProgressBar();
            this.label1 = new System.Windows.Forms.Label();
            this.labelBarra = new System.Windows.Forms.Label();
            this.buttonOnOff = new System.Windows.Forms.Button();
            this.labelAteOnde = new System.Windows.Forms.Label();
            this.labelCapacidade = new System.Windows.Forms.Label();
            this.labelVolume = new System.Windows.Forms.Label();
            this.numCentilitros = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numCentilitros)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonBase
            // 
            this.buttonBase.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonBase.Location = new System.Drawing.Point(308, 439);
            this.buttonBase.Name = "buttonBase";
            this.buttonBase.Size = new System.Drawing.Size(137, 124);
            this.buttonBase.TabIndex = 0;
            this.buttonBase.Text = "Base do copo";
            this.buttonBase.UseVisualStyleBackColor = true;
            this.buttonBase.Click += new System.EventHandler(this.buttonBase_Click);
            // 
            // pictureBox
            // 
            this.pictureBox.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox.Image")));
            this.pictureBox.ImageLocation = "";
            this.pictureBox.Location = new System.Drawing.Point(308, 60);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(137, 352);
            this.pictureBox.TabIndex = 1;
            this.pictureBox.TabStop = false;
            // 
            // progressB
            // 
            this.progressB.Location = new System.Drawing.Point(521, 487);
            this.progressB.Name = "progressB";
            this.progressB.Size = new System.Drawing.Size(190, 23);
            this.progressB.TabIndex = 2;
            this.progressB.Click += new System.EventHandler(this.progressB_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(608, 550);
            this.label1.MaximumSize = new System.Drawing.Size(5, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(5, 5);
            this.label1.TabIndex = 3;
            this.label1.Text = "label1";
            // 
            // labelBarra
            // 
            this.labelBarra.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBarra.Location = new System.Drawing.Point(545, 513);
            this.labelBarra.Name = "labelBarra";
            this.labelBarra.Size = new System.Drawing.Size(143, 21);
            this.labelBarra.TabIndex = 4;
            this.labelBarra.Text = "Nível de Enchimento";
            this.labelBarra.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonOnOff
            // 
            this.buttonOnOff.Enabled = false;
            this.buttonOnOff.Font = new System.Drawing.Font("Hobo Std", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOnOff.Location = new System.Drawing.Point(566, 114);
            this.buttonOnOff.Name = "buttonOnOff";
            this.buttonOnOff.Size = new System.Drawing.Size(97, 97);
            this.buttonOnOff.TabIndex = 5;
            this.buttonOnOff.Text = "Off";
            this.buttonOnOff.UseVisualStyleBackColor = true;
            this.buttonOnOff.Click += new System.EventHandler(this.buttonOnOff_Click);
            // 
            // labelAteOnde
            // 
            this.labelAteOnde.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAteOnde.Location = new System.Drawing.Point(69, 114);
            this.labelAteOnde.Name = "labelAteOnde";
            this.labelAteOnde.Size = new System.Drawing.Size(170, 97);
            this.labelAteOnde.TabIndex = 6;
            this.labelAteOnde.Text = "\r\n                   \r\nEncher quantos centilitros?\r\n";
            this.labelAteOnde.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelCapacidade
            // 
            this.labelCapacidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCapacidade.Location = new System.Drawing.Point(98, 487);
            this.labelCapacidade.Name = "labelCapacidade";
            this.labelCapacidade.Size = new System.Drawing.Size(144, 23);
            this.labelCapacidade.TabIndex = 8;
            this.labelCapacidade.Text = "Capacidade do copo:";
            this.labelCapacidade.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelVolume
            // 
            this.labelVolume.Enabled = false;
            this.labelVolume.Location = new System.Drawing.Point(133, 513);
            this.labelVolume.Name = "labelVolume";
            this.labelVolume.Size = new System.Drawing.Size(72, 23);
            this.labelVolume.TabIndex = 9;
            // 
            // numCentilitros
            // 
            this.numCentilitros.Location = new System.Drawing.Point(101, 173);
            this.numCentilitros.Maximum = new decimal(new int[] {
            25,
            0,
            0,
            0});
            this.numCentilitros.Name = "numCentilitros";
            this.numCentilitros.Size = new System.Drawing.Size(120, 20);
            this.numCentilitros.TabIndex = 11;
            this.numCentilitros.ValueChanged += new System.EventHandler(this.numCentilitros_ValueChanged);
            // 
            // dispensador
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(766, 625);
            this.Controls.Add(this.numCentilitros);
            this.Controls.Add(this.labelVolume);
            this.Controls.Add(this.labelCapacidade);
            this.Controls.Add(this.labelAteOnde);
            this.Controls.Add(this.buttonOnOff);
            this.Controls.Add(this.labelBarra);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.progressB);
            this.Controls.Add(this.pictureBox);
            this.Controls.Add(this.buttonBase);
            this.MinimizeBox = false;
            this.Name = "dispensador";
            this.Text = "Dispensador de Cerveja";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numCentilitros)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonBase;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.ProgressBar progressB;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelBarra;
        private System.Windows.Forms.Button buttonOnOff;
        private System.Windows.Forms.Label labelAteOnde;
        private System.Windows.Forms.Label labelCapacidade;
        private System.Windows.Forms.Label labelVolume;
        private System.Windows.Forms.NumericUpDown numCentilitros;
    }
}

