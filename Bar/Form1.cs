﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bar
{
    public partial class dispensador : Form
    {
        /// <summary>
        /// declara um novo objeto da classe Copo
        /// </summary>
        private Copo meuCopo;

        /// <summary>
        ///instancia o objeto e acciona a base para copos 
        /// </summary>
        public dispensador()
        {
            InitializeComponent();
            meuCopo = new Copo();
            buttonOnOff.Enabled = true;
        }

        /// <summary>
        /// acciona o botão de encher ou esvaziar copos
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonOnOff_Click(object sender, EventArgs e)
        {
            if (buttonOnOff.Text=="Off")
            {
                //salienta a caixa de input do nível até onde se quer encher o copo
                buttonOnOff.Text = "On";
                numCentilitros.Enabled = true;
                labelAteOnde.ForeColor = Color.Red;
            }
            else
            {
                //desativa a caixa de input e esvazia o copo
                buttonOnOff.Text = "Off";
                double valor = Convert.ToDouble(numCentilitros.Text);
                numCentilitros.Text = "0";
                numCentilitros.Enabled = false;
                labelAteOnde.ForeColor = Color.Black;
                meuCopo.Esvaziar(valor);
                progressB.Value = 0;
            }
        }

        private void progressB_Click(object sender, EventArgs e)
        {
           
        }
        private void buttonBase_Click(object sender, EventArgs e)
        {
            labelVolume.Text = Convert.ToString(meuCopo.Capacidade);
            labelBarra.Text = Convert.ToString(meuCopo.Contem);
        }

        private void numCentilitros_ValueChanged(object sender, EventArgs e)
        {
            double valor = Convert.ToDouble(numCentilitros.Text);
            meuCopo.Encher(valor);
            progressB.Value = meuCopo.ValorEmPercentagem();
        }
    }
}
